FROM node:6-slim

RUN mkdir -p /app
WORKDIR /app

COPY . /app

CMD [ "npm", "start" ]
