var truth = require('../src/true');
var assert = require('assert');
var request = require('request');
var serverURL = process.env.SERVER_URL;

it('gives 200 for health check', function(done) {
  request({
    method: 'GET',
    url: serverURL + '/ping'
  }, function(error, response, body) {
    if(error) {
      return done(error);
    }

    assert.equal(response.statusCode, 200);

    done();
  });
});
